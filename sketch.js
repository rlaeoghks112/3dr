//let width = 600;
//let height = 400;
let width = 1280;
let height = 560;
let mouseclickedx = 0;
let mouseclickedy = 0;
let mousereleasedx = 0;
let mousereleasedy = 0;
let blueprint = [];
let windows = [];
let build = false;
let floorheight = 5;
let pressed, released = false;
let speed;
let button1, button2;
let slider;
let inconsolata;
let zposition = 0;
let buttonstructuretype;
let wall;
let window_height;

function preload() {
  inconsolata = loadFont("https://cors-anywhere.herokuapp.com/https://gitlab.com/rlaeoghks112/3dr/raw/master/assets/Inconsolata.otf");
}

function setup() {
  createCanvas(width, height, WEBGL);
  textFont(inconsolata);
  textSize(11);
  frameRate(30);
  wall = true;
  button1 = createButton('build');
  button1.position(10, 40);
  button1.mousePressed(changeButton);
  button2 = createButton('clear');
  button2.position(60, 40);
  button2.mousePressed(clearfield);
  buttonstructuretype =createButton('structure type');
  buttonstructuretype.position( 110, 40 );
  buttonstructuretype.mousePressed(changestructuretype);
  slider = createSlider(0.01, 10, 0.5);
  slider.position(40, 10);
  slider.style("width", "100px");
  window_height = 150;
}

function draw() {
  stroke(255);
  background(0);

  ambientLight(255);
  text('Speed', -width/2 + 10, -height/2 + 25);
  pressed = false;
  released = false;
  speed = slider.value();

  //Line that divides two sections
  stroke(255);
  line(0, -height / 2, 0, height / 2);
  noStroke();

  //Wireframe
  ambientMaterial(255, 255, 255);
  for (var i = 0; i < blueprint.length; i += 4) {
    rect(blueprint[i], blueprint[i + 1], blueprint[i + 2] - blueprint[i], blueprint[i + 3] - blueprint[i + 1]);
  }
  ambientMaterial(255,0,0);
  for (var i = 0; i < windows.length; i += 4) {
    rect(windows[i], windows[i + 1], windows[i + 2] - windows[i], windows[i + 3] - windows[i + 1]);
  }
  
  //Orienting bases
  if ( mouseX > width/2 ) {
    camera(100, -100 + zposition, (height/2.0) / tan(PI*30.0 / 180.0), - width/2 + mouseX, -height/2 + mouseY, 0, 0, 1, 0);
  }
  else {
    camera(0, 0, (height/2.0) / tan(PI*30.0 / 180.0), 0, 0, 0, 0, 1, 0);
    zposition = 0;
  }

  //Printing bases
  stroke(0);
  ambientLight(0);
  directionalLight(100,100,100,0,0,0);
  ambientMaterial(200, 200, 200);
  translate(width / 2, 0);
  rotateX(PI / 2.5);
  rotateZ(PI / 2.5);
  translate(createVector(0, 0, -300));
  for (var i = 0; i < blueprint.length; i += 4) {
    rect(blueprint[i], blueprint[i + 1], blueprint[i + 2] - blueprint[i], blueprint[i + 3] - blueprint[i + 1]);
  }


  if (build && frameCount % 5 == 0) {
    floorheight += speed;
  }

  //printing blocks
  for (var i = 0; i < blueprint.length; i += 4) {
    quad(blueprint[i], blueprint[i + 1], floorheight, blueprint[i + 2], blueprint[i + 1], floorheight, blueprint[i + 2], blueprint[i + 3], floorheight, blueprint[i], blueprint[i + 3], floorheight);
    quad(blueprint[i], blueprint[i + 1], 0, blueprint[i], blueprint[i + 1], floorheight, blueprint[i], blueprint[i + 3], floorheight, blueprint[i], blueprint[i + 3], 0);
    quad(blueprint[i], blueprint[i + 1], 0, blueprint[i], blueprint[i + 1], floorheight, blueprint[i + 2], blueprint[i + 1], floorheight, blueprint[i + 2], blueprint[i + 1], 0);
    quad(blueprint[i + 2], blueprint[i + 3], 0, blueprint[i + 2], blueprint[i + 3], floorheight, blueprint[i + 2], blueprint[i + 1], floorheight, blueprint[i + 2], blueprint[i + 1], 0);
    quad(blueprint[i], blueprint[i + 3], 0, blueprint[i], blueprint[i + 3], floorheight, blueprint[i + 2], blueprint[i + 3], floorheight, blueprint[i + 2], blueprint[i + 3], 0);

  }
  
  if( keyIsDown( UP_ARROW) ) {
    zposition +=2;
  }
  else if ( keyIsDown( DOWN_ARROW) ) {
    zposition -=2;
  }
  
  ambientLight(255);
  ambientMaterial(200, 200, 200);
  fill(255);
  translate(0, 0, window_height);
  for ( var i = 0 ; i < windows.length ; i += 4 ) {
    if ( i != 0 ) {
      translate( (windows[i] + windows[i+2])/2 - ( windows[i-4] + windows[i-2] )/2 , (windows[i+1] + windows[i+3])/2 - ( windows[i-3] + windows[i-1] )/2 );
    }
    else {
      translate( (windows[i] + windows[i+2])/2, (windows[i+1] + windows[i+3])/2 );
    }
    //erase();
    box( abs(windows[i] - windows[i+2]), abs(windows[i+1] - windows[i+3]), 100);
    //noErase();
    var x = ( windows[i] + windows[i+2] ) / 2;
    var y = ( windows[i+1] + windows[i+3] ) / 2;
    fill(255);
  }
  translate(0, 0, -window_height);
}

function mousePressed() {
  pressed = true;
  if (mouseX < width / 2 && mouseX > 0 &&
    mouseY < height && mouseY > 0 && (!(mouseX < 180 && mouseY < 70))) {
    if ( wall ) {
      blueprint[blueprint.length] = mouseX - width / 2;
      blueprint[blueprint.length] = mouseY - height / 2;
      blueprint[blueprint.length] = mouseX - width / 2;
      blueprint[blueprint.length] = mouseY - height / 2;
    }
    else {
      windows[windows.length] = mouseX - width / 2;
      windows[windows.length] = mouseY - height / 2;
      windows[windows.length] = mouseX - width / 2;
      windows[windows.length] = mouseY - height / 2;
    }
  }
}

function mouseDragged() {
  released = true;
  if (mouseX < width / 2 && mouseX > 0 &&
    mouseY < height && mouseY > 0 && (!(mouseX < 180 && mouseY < 70))) {
    if ( wall ) {
      blueprint[blueprint.length - 2] = mouseX - width / 2;
      blueprint[blueprint.length - 1] = mouseY - height / 2;
    }
    else {
      windows[windows.length - 2] = mouseX - width / 2;
      windows[windows.length - 1] = mouseY - height / 2;
    }
  }
}

function changeButton() {
  if (build) {
    build = false;
  } else {
    build = true;
  }
}

function clearfield() {
  blueprint = [];
  windows = [];
  floorheight = 0;
}

function changestructuretype() {
  if (wall) {
    wall = false;
  } else {
    wall = true;
  }
}